const express = require('express')
const bodyParser = require('body-parser')

const fs = require('fs-extra')
const path = require('path')

const staticDir = path.join(__dirname, 'static')
const responseDir = path.join(__dirname, 'responses')
fs.ensureDirSync(responseDir)

const app = express()
app.use(express.static(staticDir))
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/responses', async (req, res) => {
  const directoryListing = await fs.readdir(responseDir)
  return res.send(`<p>Response count: ${directoryListing.length}</p>`)
})

app.post('/send', async (req, res) => {
  const size = Object.values(req.body).join('').length
  const now = (new Date()).toString()
  console.log(`[${now}] received ${size} characters`)

  // Assume they'll type more than 10 characters
  if (size < 10) {
    return res.sendFile(path.join(staticDir, 'empty.html'))
  }

  const outFile = path.join(responseDir, now + '.txt')
  await fs.writeFile(outFile, JSON.stringify(req.body, null, 2))
  return res.sendFile(path.join(staticDir, 'thanks.html'))
})

app.listen(3000, () => {
  console.log('Running on 3000')
})
