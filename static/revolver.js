// TODO:
// The text revolver runs even when the element is out of focus
// Use IntersectionObserver API to fix that

// observer = new IntersectionObserver((entry, observer) => {
//   console.log('Entry:', entry)
//   console.log('Observer:', observer)
//   if (entry.intersectionRatio > 0) {
//     console.log('Is in view')
//   } else {
//     console.log('No longer in view')
//   }
// })
// observer.observe(document.querySelector('header'))

const revolver = document.querySelector('#revolver-thing')
const text = [
  'fun.',
  'the world needs.',
  'new.',
  'your friends want.',
  'better.',
  'you need.',
  'inspirational.',
  'for the future.',
  '.',
  // This will be a loop so add what was hardcoded too
  revolver.innerText,
]
const writingSpeed = 70
const backspaceSpeed = 50

console.log('Text:', text)
// Start at this element in the text array: text[0]
let index = 0
// Call the main loop 1 second after page load
setTimeout(loop, 1000)

function loop() {
  backspace()

  const letters = text[index++ % text.length].split('')
  setTimeout(() => writeLetters(letters), 1000)

  // Call itself again in 5 seconds
  setTimeout(loop, 5000)
}

function backspace() {
  const text = revolver.innerText
  // Can a character be removed?
  if (text.length == 0) {
    return
  }
  revolver.innerText = text.slice(0, -1)
  setTimeout(backspace, backspaceSpeed)
}

function writeLetters(letters) {
  if (letters.length == 0) {
    return
  }
  const [head, ...remainingLetters] = letters
  revolver.innerText += head
  setTimeout(() => writeLetters(remainingLetters), writingSpeed)
}
